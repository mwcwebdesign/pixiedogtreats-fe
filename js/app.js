new Vue({
    el: '#login',
    data: {
        user: {
            fname: "Mike",
            lname: "Walton",
            email: "mike@pixiesdogtreats.com",
            cart: {
                itemno: "pt0008",
                itemno: "pt0007"
            }
        }
    },
    methods: {
        isLoggedIn: function (e) {
            if(document.cookie.indexOf("username=") >= 0) {
                console.log("Is Logged In")
                return true
            } else {
                console.log("Is Not Logged In")
                return false;
            }
        },
        logout: function (e) {
            document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
            window.location.href = "index.html"
        }
    }
})

new Vue({
    el: '#signin',
    data: {
		user: null,
		password: null,
		feedback: null
    },
    methods: {
        login: function (e) {
            var config = {
                header: {'Content-Type': 'text/plain'}
            }

            axios.post('/api/authenticateUser/', 
			{
				email: this.user,
				password: this.password
            }, config)
            .then(res => {
                console.log(res)
                if (res.data == "null") {
                    this.feedback = "Wrong username or password"
                } else {
                    this.feedback = null
                    document.cookie = "username="+this.user
                    window.location.href = "index.html"
                }   
            })
			
			
			
			//document.cookie = "username="+document.username
            //window.location.href = "index.html"
        }
    }
})




new Vue({
    el: '#products',
    data: {
        products: []
    },
	created: function () {
		axios.get('/api/getProducts')
		  .then(response => {
			  this.products = response.data
			  console.log(this.products)
		  })
	},
	methods: {
        isLoggedIn: function (e) {
            if(document.cookie.indexOf("username=") >= 0) {
                return true
            } else {
                return false;
            }
        },
		addToCart: function(itemNo, event) {
			event.preventDefault()
			console.log(itemNo)
		}
	}
})




new Vue({
    el: '#cart',
    data: {

    },
    methods: {

    }
})